# Privacy Policy for Air Runner

## Introduction

We, the developers of the Air Runner app, respect your privacy and are committed to protecting your personal data. This Privacy Policy explains how we handle your data when you use our app.

## Data Collection

The Air Runner app does not collect any personal data from users. We do not require registration or the input of any personal information to use the app.

## Internet Access

The Air Runner app does not have access to the internet. All features and functionalities of the app operate entirely offline, providing additional protection for your privacy.

## Feedback

If you have any questions or comments regarding our Privacy Policy, please contact us at the following email address:

**Email:** aleksnc.ip@gmail.com

We are ready to answer any of your questions and consider any suggestions or comments.

## Changes to the Privacy Policy

We reserve the right to make changes to this Privacy Policy. Any changes will be published on this page. We recommend periodically checking our Privacy Policy to stay informed of any updates.

Last updated: May 31, 2024

Thank you for using the Air Runner app!
